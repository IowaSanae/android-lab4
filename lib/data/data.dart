import 'package:lab4/models/category.dart';

const dataCategories = [
  Category(1, "Android", "https://logos-world.net/wp-content/uploads/2021/08/Android-Logo-2014-2019.png"),
  Category(2, "iOS", "https://1000logos.net/wp-content/uploads/2017/02/iOS-Logo-2013.jpg"),
  Category(3, "Windows", "https://www.bleepstatic.com/content/hl-images/2020/10/13/Windows-10-headpic.jpg"),
  Category(4, "Linux", "https://www.famouslogos.us/images/linux-logo.jpg"),
];
