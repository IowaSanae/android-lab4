class Products{
  final int id;
  final String content;
  final String image;

  const Products(this.id, this.content, this.image);
}