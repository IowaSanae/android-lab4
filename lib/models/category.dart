import 'package:flutter/cupertino.dart';

class Category{
  final int id;
  final String content;
  final String image;

  const Category(@required this.id, @required this.content, this.image);
}
