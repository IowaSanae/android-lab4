import 'package:flutter/material.dart';
import 'package:lab4/models/category.dart';

class Items extends StatelessWidget {
  late Category category;
  Items(this.category, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print(category.content);
        Navigator.pushNamed(
            context, '/FoodPage', arguments: {'category': category});
      },
      child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(category.image),
                fit: BoxFit.cover
            ),
            borderRadius: BorderRadius.circular(10)
        ),
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.all(8),
            color: Colors.blue.withOpacity(.5),
            child: Text(
              category.content,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
